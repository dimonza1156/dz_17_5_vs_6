#include <iostream>
#include <math.h>


class Vector
{
public:
	Vector() : x(1), y(1), z(1)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void ShowVector()
	{
		std::cout << "Vector coordinates x: " << x << std::endl;
		std::cout << "Vector coordinates y: " << y << std::endl;
		std::cout << "Vector coordinates z: " << z << std::endl;
	}

	double VectorModule()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
private:
	double x = 0;
	double y = 0;
	double z = 0;
};

int main()
{
	Vector v1(2, 11, 14);

	v1.ShowVector();
	std::cout << "Vector module " << v1.VectorModule() << std::endl;

	return 0;
}